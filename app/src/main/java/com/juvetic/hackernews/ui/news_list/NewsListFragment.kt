package com.juvetic.hackernews.ui.news_list


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.juvetic.hackernews.R
import com.juvetic.hackernews.utils.Errors
import com.juvetic.hackernews.utils.isConnected
import com.juvetic.hackernews.utils.setTopDrawable
import com.juvetic.hackernews.vo.Item
import kotlinx.android.synthetic.main.fragment_news_list.*
import kotlinx.android.synthetic.main.list_item_story_multiple_shimmer.*
import kotlinx.android.synthetic.main.view_error.*
import kotlinx.coroutines.*
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import kotlin.coroutines.CoroutineContext


class NewsListFragment : Fragment(), CoroutineScope {

    val supervisor = SupervisorJob()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + supervisor

    private val newsTypeViewModel by lazy {
        ViewModelProviders
            .of(this@NewsListFragment)
            .get(NewsListViewModel::class.java)
    }

    private var retrySnack: Snackbar? = null

    private val mutableItemList = mutableListOf<Item?>()

    companion object {
        fun newInstance(): NewsListFragment {
            return NewsListFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_news_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        launch {

            retrySnack = Snackbar.make(newsTypeContainer, "You are offline", Snackbar.LENGTH_INDEFINITE)
                .setAction("Retry") {
                    retryAction()
                }

            newsTypeViewModel.isLoading.observe(viewLifecycleOwner, Observer {
                if (it) {
                    if (isConnected(requireContext())) {
                        loadMoreStories(mutableItemList)
                    } else {
                        newsTypeViewModel.isLoading.value = false
                        retrySnack?.setText(resources.getString(R.string.no_internet))?.show()
                    }
                }
            })
            loadStories(false)
            newsTypeContainer.apply {
                setProgressViewOffset(true, 100, 250)
                setOnRefreshListener { loadStories(true) }
            }

        }
    }

    private fun retryAction() {
        if (isConnected(requireContext())) {
            loadStories(true)
        } else {
            launch {
                delay(300)
                retrySnack?.setText(resources.getString(R.string.no_internet))?.show()
            }
        }
    }

    private fun loadStories(refresh: Boolean) {
        launch {
            try {
                showStoryItems()

                setLoadView(true)

                val itemList = newsTypeViewModel.getStoriesAsync(refresh).await()
                mutableItemList.apply {
                    clear()
                    addAll(itemList)
                }

                setLoadView(false)

                rvItems.adapter?.notifyDataSetChanged() ?: setUpRecyclerView(mutableItemList)
                newsTypeContainer.isRefreshing = false

            } catch (u: UnknownHostException) {
                showOfflineView()
            } catch (ce: ConnectException) {
                showOfflineView()
            } catch (se: SocketTimeoutException) {
                showErrorView()
            } catch (e: Errors) {
                when (e) {
                    is Errors.OfflineException -> showOfflineView()
                    is Errors.FetchException -> showErrorView()
                }
            } finally {
                shimmerNewsType?.apply {
                    stopShimmer()
                    isVisible = false
                }
            }
        }
    }

    private fun showErrorView() {
        rvItems.isVisible = false
        tvError.apply {
            isVisible = true
            text = resources.getString(R.string.error)
            val drawable = resources.getDrawable(R.drawable.ic_error_black_24dp)
            setTopDrawable(drawable)
        }
        newsTypeContainer.isRefreshing = false
        retrySnack?.setText(resources.getString(R.string.error))?.show()

    }

    private fun showOfflineView() {
        rvItems.isVisible = false
        tvError.apply {
            isVisible = true
            text = resources.getString(R.string.no_internet)
            val drawable = resources.getDrawable(R.drawable.ic_signal_wifi_off_black_24dp)
            setTopDrawable(drawable)
        }
        newsTypeContainer.isRefreshing = false
        retrySnack?.setText(resources.getString(R.string.no_internet))?.show()

    }

    private fun showStoryItems() {
        tvError.isVisible = false
        rvItems.isVisible = true
        retrySnack?.dismiss()
    }

    private fun setLoadView(isLoading: Boolean) {
        shimmerNewsType.apply {
            if (isLoading) startShimmer() else stopShimmer()

            isVisible = isLoading
        }
        rvItems.isVisible = !isLoading
    }

    private fun setUpRecyclerView(mutableItemList: MutableList<Item?>) {

        rvItems.apply {
            isVisible = true
            adapter = NewsItemAdapter(
                mutableItemList,
                requireActivity()
            )
            layoutManager =
                LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false).apply {
                    scrollToPosition(newsTypeViewModel.scrollPosition)
                }
            addItemDecoration(
                DividerItemDecoration(requireContext(), RecyclerView.VERTICAL)
            )

            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    val layoutManager = rvItems.layoutManager as LinearLayoutManager

                    newsTypeViewModel.scrollPosition = layoutManager.findFirstCompletelyVisibleItemPosition()

                    if (layoutManager.findLastCompletelyVisibleItemPosition() == mutableItemList.size - 1) {
                        if (newsTypeViewModel.isNotFullLoaded() &&
                            newsTypeViewModel.isLoading.value == false
                        ) {
                            if (isConnected(requireContext())) {
                                retrySnack?.dismiss()
                                newsTypeViewModel.isLoading.value = true
                            } else {
                                retrySnack?.setText(resources.getString(R.string.no_internet))?.show()
                            }
                        }
                    }
                }
            })
        }
    }

    private fun loadMoreStories(mutableItemList: MutableList<Item?>) {
        launch {
            try {

                addDummyLoadItem(true)
                val newsItemList = newsTypeViewModel.loadMoreItemsAsync().await()
                addDummyLoadItem(false)

                if (newsItemList.isNotEmpty()) {
                    mutableItemList.addAll(newsItemList)
                    rvItems.adapter?.notifyDataSetChanged()
                }
            } catch (u: UnknownHostException) {
                addDummyLoadItem(false)
                retrySnack?.setText(resources.getString(R.string.no_internet))?.show()
            } catch (ce: ConnectException) {
                addDummyLoadItem(false)
                retrySnack?.setText(resources.getString(R.string.no_internet))?.show()
            } catch (se: SocketTimeoutException) {
                addDummyLoadItem(false)
                retrySnack?.setText(resources.getString(R.string.error))?.show()
            } finally {
                newsTypeViewModel.isLoading.value = false
            }
        }

    }

    private fun addDummyLoadItem(toAdd: Boolean) {
        if (toAdd) {
            mutableItemList.add(null)
            rvItems.adapter?.notifyItemInserted(mutableItemList.size - 1)
        } else {
            mutableItemList.removeAt(mutableItemList.size - 1)
            rvItems.adapter?.notifyItemRemoved(mutableItemList.size - 1)
        }
    }

    override fun onDetach() {
        coroutineContext.cancelChildren()
        retrySnack?.dismiss()
        super.onDetach()
    }
}
