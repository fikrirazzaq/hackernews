package com.juvetic.hackernews.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.juvetic.hackernews.R
import com.juvetic.hackernews.ui.news_list.NewsListFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancelChildren
import kotlin.coroutines.CoroutineContext

class MainActivity : AppCompatActivity(), CoroutineScope {

    private val superVisor = SupervisorJob()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + superVisor

    private val fragTag = "Top"
    private val toolbarTitle = "Top News"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setFragment()
    }

    private fun setFragment() {
        setToolBar(toolbarTitle)
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, NewsListFragment.newInstance(), fragTag).commit()
    }

    private fun setToolBar(type: String) {
        tvToolbarTitle.text = type
    }

    override fun onDestroy() {
        coroutineContext.cancelChildren()
        super.onDestroy()
    }

}
