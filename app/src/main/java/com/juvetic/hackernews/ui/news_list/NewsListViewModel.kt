package com.juvetic.hackernews.ui.news_list

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.juvetic.hackernews.api.NewsClient
import com.juvetic.hackernews.db.NewsDb
import com.juvetic.hackernews.repository.NewsRepository
import com.juvetic.hackernews.utils.Errors
import com.juvetic.hackernews.utils.getSafeResponse
import com.juvetic.hackernews.utils.isConnected
import com.juvetic.hackernews.vo.Item
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async

class NewsListViewModel(application: Application) : AndroidViewModel(application) {

    private val itemsRepository: NewsRepository
    private val client = NewsClient(application).newsService

    private var typeStories = mutableListOf<Item?>()
    private var itemIds: List<Long> = emptyList()
    private val mutableListItems = mutableListOf<Item?>()

    val isLoading: MutableLiveData<Boolean> = MutableLiveData(false)
    var scrollPosition = 0

    private val batchSize = 10
    private var start = 0
    private var end = batchSize

    init {
        val itemDao = NewsDb.getDatabase(application).newsDao()
        itemsRepository = NewsRepository(itemDao)
    }

    fun getStoriesAsync(isRefresh: Boolean): Deferred<List<Item?>> {
        return viewModelScope.async {
            if (typeStories.isNotEmpty() && !isRefresh) {
                return@async typeStories
            } else {
                if (isConnected(getApplication())) {
                    itemIds = getSafeResponse(client.topStories())
                    end = if (batchSize >= itemIds.size) itemIds.size - 1 else batchSize
                    val itemList = getItemListBatchAsync(IntRange(start, end)).await()
                    mutableListItems.addAll(itemList)
                    typeStories = mutableListItems
                    typeStories
                } else {
                    throw Errors.OfflineException()
                }
            }
        }
    }


    private fun getItemListBatchAsync(range: IntRange): Deferred<List<Item>> {
        return viewModelScope.async(Dispatchers.IO) {
            val itemList = mutableListOf<Item>()

            if (itemIds.isNotEmpty()) {
                for (i in range) {
                    val id = itemIds[i]
                    val item = client.getItem(id).body()
                    item?.let { itemList.add(it) }
                }
            }

            itemList
        }
    }

    fun loadMoreItemsAsync(): Deferred<List<Item>> {
        return viewModelScope.async {
            if (end < itemIds.size - 1) {
                start = end + 1
                end =
                    if (end + batchSize < itemIds.size) end + batchSize
                    else itemIds.size - 1

                val itemList = getItemListBatchAsync(IntRange(start, end)).await()
                mutableListItems.addAll(itemList)
                typeStories = mutableListItems
                return@async itemList
            } else {
                return@async emptyList<Item>()
            }
        }
    }

    fun isNotFullLoaded(): Boolean =
        if (itemIds.isNotEmpty())
            end < itemIds.size - 1
        else
            false

}