package com.juvetic.hackernews.ui.news_detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.juvetic.hackernews.R
import com.juvetic.hackernews.utils.Errors
import com.juvetic.hackernews.utils.translate
import com.juvetic.hackernews.vo.Item
import com.squareup.picasso.Picasso
import com.juvetic.hackernews.vo.CommentItem
import kotlinx.android.synthetic.main.activity_news_detail.*
import kotlinx.android.synthetic.main.list_item_comment.*
import kotlinx.android.synthetic.main.list_item_comment_load.view.*
import kotlinx.coroutines.*
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import kotlin.coroutines.CoroutineContext

class NewsDetailActivity : AppCompatActivity(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    private var isSaved: Boolean = false

    private val commentsList = mutableListOf<CommentItem?>()
    private lateinit var commentsAdapter: CommentsAdapter

    private val newsDetailsViewModel by lazy {
        ViewModelProviders.of(this).get(NewsDetailViewModel::class.java)
    }

    private val childCommentListener = object :
        CommentsAdapter.ChildCommentListener {

        var childCommentJob: Job? = null

        override fun onExpand(commentItem: CommentItem, rv: RecyclerView, loader: View, depth: Int) {
            childCommentJob = loadChildComments(commentItem, rv, loader, depth)
        }

        override fun onCollapse(rv: RecyclerView, loader: View) {
            childCommentJob?.cancel()
            rv.translate(-rv.height * 1f, false)
            loader.isVisible = false
            loader.divideContainerLoad.removeAllViews()
        }

    }

    private var retrySnack: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_detail)

        retrySnack = Snackbar.make(root, resources.getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
            .setAction("Retry") {
                loadComments(true)
            }

        setCollapsingToolBarLayout()

        val item = getStoryItem()
        val kids = item.kids
        tvCommentLabel.text = "${item.kids?.size} comments"
        if (kids != null) {
            newsDetailsViewModel.commentIds = kids
            loadComments(false)
        } else {
            tvNoComments.isVisible = true
        }
    }

    private fun showMessageView(msg: String) {
        tvNoComments.apply {
            isVisible = true
            text = msg
        }
        commentsLoadContainer.isVisible = false
        rvComments.isVisible = false
        launch {
            delay(300)
            retrySnack?.setText(msg)?.show()
        }
    }

    private fun loadComments(isRefresh: Boolean) {
        launch {
            try {
                tvNoComments.isVisible = false
                val item = getStoryItem()

                newsDetailsViewModel.isLoading.observe(this@NewsDetailActivity, Observer {
                    if (it) {
                        loadMoreComments()
                    }
                })
                commentsLoadContainer.isVisible = true
                val list = newsDetailsViewModel.getCommentsAsync(isRefresh).await()
                commentsLoadContainer.isVisible = false

                commentsList.addAll(list)
                commentsAdapter =
                    CommentsAdapter(commentsList, childCommentListener)
                rvComments.apply {
                    isVisible = true
                    adapter = commentsAdapter
                    layoutManager = LinearLayoutManager(
                        this@NewsDetailActivity,
                        RecyclerView.VERTICAL, false
                    )
                    addItemDecoration(
                        DividerItemDecoration(this@NewsDetailActivity, RecyclerView.VERTICAL)
                    )
                }

                storyContainer.setOnScrollChangeListener { v: NestedScrollView, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
                    newsDetailsViewModel.scrollY = scrollY
                    newsDetailsViewModel.scrollX = scrollX
                    if (scrollY > oldScrollY) {
                        val lastItemY = rvComments.measuredHeight - v.measuredHeight
                        if (scrollY > lastItemY) {
                            if (newsDetailsViewModel.isNotFullLoaded() &&
                                newsDetailsViewModel.isLoading.value == false
                            ) {
                                newsDetailsViewModel.isLoading.value = true
                            }
                        }
                    }
                }
                storyContainer.post {
                    storyContainer.scrollTo(newsDetailsViewModel.scrollX, newsDetailsViewModel.scrollY)
                }
            } catch (u: UnknownHostException) {
                showMessageView(resources.getString(R.string.no_internet))
            } catch (ce: ConnectException) {
                showMessageView(resources.getString(R.string.no_internet))
            } catch (se: SocketTimeoutException) {
                showMessageView(resources.getString(R.string.request_timeout))
            } catch (e: Errors) {
                when (e) {
                    is Errors.OfflineException -> showMessageView(resources.getString(R.string.no_internet))
                    is Errors.FetchException -> showMessageView(e.message ?: resources.getString(R.string.error))
                }
            }
        }
    }


    private fun loadMoreComments() {
        launch {

            try {
                addDummyLoadItem(true)
                val moreComments = newsDetailsViewModel.getMoreCommentsAsync().await()
                addDummyLoadItem(false)

                commentsList.apply {
                    clear()
                    addAll(moreComments)
                }
                commentsAdapter.notifyDataSetChanged()

                newsDetailsViewModel.isLoading.value = false
            } catch (u: UnknownHostException) {
                showMessageView(resources.getString(R.string.no_internet))
            } catch (ce: ConnectException) {
                showMessageView(resources.getString(R.string.no_internet))
            } catch (se: SocketTimeoutException) {
                showMessageView(resources.getString(R.string.request_timeout))
            } catch (e: Errors) {
                when (e) {
                    is Errors.OfflineException -> showMessageView("You are offline")
                    is Errors.FetchException -> showMessageView(e.message ?: resources.getString(R.string.error))

                }
            }
        }
    }

    private fun addDummyLoadItem(toAdd: Boolean) {
        if (toAdd) {
            commentsList.add(null)
            commentsAdapter.notifyItemInserted(commentsList.size - 1)
        } else {
            commentsList.removeAt(commentsList.size - 1)
            commentsAdapter.notifyItemInserted(commentsList.size - 1)
        }
    }

    private fun loadChildComments(commentItem: CommentItem, rv: RecyclerView, loader: View, depth: Int): Job {
        return launch {
            try {

                val lf = LayoutInflater.from(this@NewsDetailActivity)
                loader.isVisible = true

                for (i in 0 until depth) {
                    val view = lf.inflate(R.layout.view_separator, commentContainer, false)
                    loader.divideContainerLoad.addView(view)
                }

                val childComments = newsDetailsViewModel.getChildCommentsAsync(commentItem).await()
                loader.isVisible = false

                val commentsAdapter =
                    CommentsAdapter(childComments, childCommentListener, depth)
                rv.apply {
                    rv.isVisible = true
                    adapter = commentsAdapter
                    layoutManager = LinearLayoutManager(
                        this@NewsDetailActivity,
                        RecyclerView.VERTICAL, false
                    )
                    addItemDecoration(
                        DividerItemDecoration(this@NewsDetailActivity, RecyclerView.VERTICAL)
                    )
                    translate(rv.height * 1f, true)
                }

            } catch (e: Exception) {
                when (e) {
                    is UnknownHostException,
                    is ConnectException,
                    is Errors.OfflineException,
                    is SocketTimeoutException -> {
                        loader.isVisible = false
                        Snackbar.make(root, resources.getString(R.string.no_internet), Snackbar.LENGTH_LONG).show()
                    }
                    else -> throw  e
                }
            }
        }
    }

    private fun setCollapsingToolBarLayout() {
        val logoUrl = intent.getStringExtra("LogoUrl")

        val item = getStoryItem()
        Picasso.get().load(logoUrl).placeholder(R.drawable.ic_comment_black_24dp).into(ivLogoDetails)

        collapsingToolbar.title = item.title

        tvAuthor.text = "By ${item.by}"

        setSupportActionBar(toolBar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun getStoryItem() = intent.getSerializableExtra("itemObj") as Item


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item!!)
    }


}
