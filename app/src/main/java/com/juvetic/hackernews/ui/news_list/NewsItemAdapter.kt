package com.juvetic.hackernews.ui.news_list

import android.app.Activity
import android.app.ActivityOptions
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.juvetic.hackernews.R
import com.juvetic.hackernews.ui.news_detail.NewsDetailActivity
import com.juvetic.hackernews.utils.LOGO_URL
import com.juvetic.hackernews.vo.Item
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_story.view.*


class NewsItemAdapter(
    private val newsItemList: List<Item?>,
    private val parentActivity: Activity
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val picasso = Picasso.get()

    private val VIEW_STORY = 0
    private val VIEW_LOAD = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return if (viewType == VIEW_LOAD) {
            val view = inflater.inflate(R.layout.list_item_story_load, parent, false)
            LoadViewHolder(view)
        } else {
            val view = inflater.inflate(R.layout.list_item_story, parent, false)
            ViewHolder(view)
        }
    }

    override fun getItemCount() = newsItemList.size

    override fun getItemViewType(position: Int): Int {
        val item = newsItemList[position]
        return if (item == null) VIEW_LOAD else VIEW_STORY
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ViewHolder -> {
                val item = newsItemList[position]
                item?.let { holder.bind(it) }
            }
        }
    }

    inner class LoadViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Item) {
            with(itemView) {

                itemView.setOnClickListener {

                    val sharedIv = ivLogoNews

                    val options = ActivityOptions.makeSceneTransitionAnimation(
                        parentActivity,
                        sharedIv, "logoTransition"
                    )
                    context.startActivity(
                        Intent(context, NewsDetailActivity::class.java).apply {
                            putExtra("LogoUrl", "$LOGO_URL${item.domain}")
                            putExtra("url", item.url)
                            putExtra("author", item.by)
                            putExtra("itemObj", item)
                        },
                        options.toBundle()
                    )
                }

                tvTitleNormal.text = item.title
                tvAuthorNormal.text = item.by
                tvCommentNormal.text = item.descendants.toString()
                tvScoreNormal.text = item.score.toString()
                picasso.load("$LOGO_URL${item.domain}").placeholder(R.drawable.ic_comment_black_24dp).into(ivLogoNews)
            }
        }
    }
}