package com.juvetic.hackernews.repository

import androidx.annotation.WorkerThread
import com.juvetic.hackernews.db.NewsDao
import com.juvetic.hackernews.vo.Item

class NewsRepository(private val itemsDao: NewsDao) {

    val savedStories = itemsDao.getSavedStories()

    @WorkerThread
    suspend fun insertStory(item: Item) {
        itemsDao.insertStory(item)
    }

    @WorkerThread
    suspend fun deleteStory(item: Item) {
        itemsDao.deleteStory(item)
    }

    @WorkerThread
    suspend fun getItemId(id: Long): Long? = itemsDao.getItemId(id)
}