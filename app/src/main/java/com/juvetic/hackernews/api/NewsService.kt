package com.juvetic.hackernews.api

import com.juvetic.hackernews.vo.Item
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface NewsService {

    @GET("topstories.json")
    suspend fun topStories(): Response<List<Long>>

    @GET("item/{id}.json")
    suspend fun getItem(@Path("id") id: Long): Response<Item>
}