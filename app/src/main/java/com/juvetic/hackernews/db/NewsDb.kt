package com.juvetic.hackernews.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.juvetic.hackernews.vo.Item

@Database(entities = [Item::class], version = 1)
@TypeConverters(NewsTypeConverters::class)
abstract class NewsDb : RoomDatabase() {
    abstract fun newsDao(): NewsDao

    companion object {
        @Volatile
        private var instance: NewsDb? = null

        fun getDatabase(context: Context): NewsDb {
            if (instance != null) {
                return instance as NewsDb
            }
            synchronized(this) {
                instance = Room.databaseBuilder(
                    context.applicationContext,
                    NewsDb::class.java,
                    "news_db"
                ).build()
                return instance as NewsDb
            }
        }
    }

}