package com.juvetic.hackernews.vo

data class CommentItem(
    val item: Item,
    var child:List<CommentItem>?,
    var isExpanded:Boolean = false
)